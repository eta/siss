(defsystem "siss"
  :depends-on ("usocket" "bordeaux-threads" "cl+ssl" "cl-ppcre" "drakma" "local-time" "split-sequence" "trivial-backtrace")
  :serial t
  :build-operation "program-op"
  :build-pathname "siss"
  :entry-point "siss::main"
  :components
  ((:file "packages")
   (:file "globals")
   (:file "ssl-hackery")
   (:file "smtp-parse")
   (:file "siss")))

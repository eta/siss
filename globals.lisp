;; Global variables / config

(in-package :siss)

(defparameter *max-size-bytes*
  (* 1024 1024 10) ; 10 MiB
  "Maximum message payload size, in bytes")
(defparameter *max-spamd-content-length*
  nil
  "Maximum message payload size that SpamAssassin is allowed to send back to us.")
(defparameter *our-fqdn*
  nil
  "The FQDN of this mailserver.")
(defparameter *our-mail-domain*
  nil
  "The domain name for emails this mail server accepts.")
(defparameter *rt-gateway-url*
  nil
  "The URL of a RT instance's mail gateway.")
(defparameter *ssl-cert-path*
  nil
  "Path to a PEM-encoded certificate.")
(defparameter *ssl-key-path*
  nil
  "Path to a PEM-encoded private key.")
(defparameter *default-rt-queue*
  "Emails"
  "Catch-all queue for random emails")
(defparameter *spam-rt-queue*
  "Spam"
  "Queue for emails determined to be spam.")
(defparameter *recipient-queue-mappings*
  nil
  "alist of recipient localparts that get a special queue")
(defparameter *sa-host*
  nil
  "Hostname of a SpamAssassin spamd server.")
(defparameter *sa-port*
  nil
  "Port of a SpamAssassin spamd server.")
(defparameter *sa-timeout*
  5
  "Maximum number of seconds to wait for SpamAssassin checks.")
(defparameter *listen-host*
  "0.0.0.0"
  "Hostname to listen for incoming SMTP connections on.")
(defparameter *listen-port*
  25
  "Port to listen for incoming SMTP connections on.")
(defparameter *enable-proxy-protocol*
  nil
  "Whether or not to enable the PROXY protocol.")

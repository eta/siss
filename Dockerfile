FROM rigetti/lisp AS siss-compiled

RUN apt update -y && apt install -y git
ADD ./ /src/siss
WORKDIR /src/siss
RUN make

FROM debian:stable-slim AS siss
RUN mkdir /siss
WORKDIR /siss
RUN apt-get update && apt-get install -y libz-dev libssl-dev ca-certificates
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
COPY --from=siss-compiled /src/siss/siss /siss/
ENTRYPOINT "/siss/siss"

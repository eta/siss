;;; =================
;;; BEGIN SSL HACKERY
;;; =================
;;; FIXME: CL+SSL doesn't load the whole certificate chain, which is Bad Practice.
;;; We fix it to use the correct OpenSSL function that does the needful.

(cl+ssl::define-ssl-function ("SSL_use_certificate_chain_file" cl+ssl::ssl-use-certificate-chain-file)
  :int
  (ctx cl+ssl::ssl-pointer)
  (str :string))

(defun cl+ssl::install-key-and-cert (handle key certificate)
  (when certificate
    (unless (eql 1 (cl+ssl::ssl-use-certificate-chain-file handle
                                                           certificate))
      (error 'cl+ssl::ssl-error-initialize
             :reason (format nil "Can't load certificate ~A" certificate))))
  (when key
    (unless (eql 1 (cl+ssl::ssl-use-privatekey-file handle
                                                    key
                                                    cl+ssl::+ssl-filetype-pem+))
      (error 'cl+ssl::ssl-error-initialize :reason (format nil "Can't load private key file ~A" key)))))

;;; ===============
;;; END SSL HACKERY
;;; ===============

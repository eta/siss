;; SMTP command parsing

(in-package :siss)

(defclass starttls () ())

(defclass helo-or-ehlo ()
  ((supplied-domain
    :initarg :supplied-domain
    :accessor supplied-domain)))

(defclass ehlo (helo-or-ehlo) ())

(defclass mail-from ()
  ((mail-addr
    :initarg :mail-addr
    :accessor mail-addr)
   (mail-size
    :initarg :mail-size
    :initform 0
    :accessor mail-size)))

(defclass rcpt-to ()
  ((mail-addr
    :initarg :mail-addr
    :accessor mail-addr)))

(defclass cmd-data () ())
(defclass cmd-quit () ())
(defclass cmd-rset () ())
(defclass cmd-noop () ())

(defparameter +mail-from-regex+
  (cl-ppcre:create-scanner "mail from: *<([^@]+@[^>]+)>"
                           :case-insensitive-mode t))
(defparameter +rcpt-to-regex+
  (cl-ppcre:create-scanner "rcpt to: *<([^@]+@[^>]+)>"
                           :case-insensitive-mode t))
(defparameter +mail-size-regex+
  (cl-ppcre:create-scanner "size=([0-9]+)"
                           :case-insensitive-mode t))
(defparameter +rt-ticket-regex+
  (cl-ppcre:create-scanner "ticket: ([0-9]+)"
                           :multi-line-mode t
                           :case-insensitive-mode t))
(defparameter +proxy-address-regex+
  (cl-ppcre:create-scanner "tcp[46] ([^ ]+)"
                           :case-insensitive-mode t))

(defun split-at (seq pos &optional (consume-value-at-pos-p t))
  "Splits SEQ at POS, returning two values. If CONSUME-VALUE-AT-POS-P is truthy, throws away the element at POS."
  (declare (type sequence seq) (type fixnum pos))
  (cond
    ((> pos (length seq)) (error "Position is outside sequence."))
    ((eql pos (length seq)) (values seq nil))
    (t (values (subseq seq 0 pos) (subseq seq (+ pos (if consume-value-at-pos-p 1 0)))))))

(defun split-address (addr)
  "Splits ADDR (a@b), and returns (VALUES a b)"
  (let ((at-sign (position #\@ addr)))
    (split-at addr at-sign)))

(defun extract-proxy-source-address (proxy-line)
  "Extracts a proxy source address from PROXY-LINE, a PROXY protocol line/"
  (cl-ppcre:register-groups-bind (src-addr)
      (+proxy-address-regex+ proxy-line :sharedp t)
    src-addr))

(defun extract-rt-ticket-id (response)
  "Extracts a RT ticket ID from RESPONSE."
  (cl-ppcre:register-groups-bind (ticket-id)
      (+rt-ticket-regex+ response :sharedp t)
    (when ticket-id
      (parse-integer ticket-id))))

(defun extract-size (line)
  "Extracts a size parameter from the MAIL FROM line LINE, returning the mail size in bytes, if provided."
  (cl-ppcre:register-groups-bind (size)
      (+mail-size-regex+ line :sharedp t)
    (when size
      (parse-integer size))))

(defun extract-address (regex line)
  "Extracts an email address from LINE using REGEX, returning the address contained within, if any."
  (cl-ppcre:register-groups-bind (address)
      (regex line :sharedp t)
    address))

(defun parse-smtp-command (line)
  "Parses the SMTP command LINE, or returns NIL if the command is malformed."
  (declare (type string line))
  (let* ((space-pos (position #\Space line))
         (command (string-upcase (subseq line 0 space-pos)))
         (args (when (and space-pos (> (length line) (1+ space-pos)))
                 (subseq line (1+ space-pos)))))
    (block nil
      (cond
        ((string= command "DATA") (make-instance 'cmd-data))
        ((string= command "QUIT") (make-instance 'cmd-quit))
        ((string= command "RSET") (make-instance 'cmd-rset))
        ((string= command "NOOP") (make-instance 'cmd-noop))
        ((string= command "STARTTLS") (make-instance 'starttls))
        ((not args) nil)
        ((string= command "MAIL")
         (let ((address (extract-address +mail-from-regex+ line)))
           (when address
             (make-instance 'mail-from
                            :mail-addr address
                            :mail-size (extract-size line)))))
        ((string= command "RCPT")
         (let ((address (extract-address +rcpt-to-regex+ line)))
           (when address
             (make-instance 'rcpt-to
                            :mail-addr address))))
        ((position #\Space args) nil)
        ((or (string= command "EHLO") (string= command "HELO"))
         (let ((class-name (if (string= command "EHLO") 'ehlo 'helo-or-ehlo)))
           (make-instance class-name :supplied-domain args)))
        (t nil)))))
